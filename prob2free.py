<<<<<<< HEAD
#!/usr/bin/env python
# -*- coding: utf-8 -*-
# comments

from optparse import OptionParser
from time import localtime, strftime
import numpy as np
from scipy.optimize import curve_fit
from numpy import exp
import matplotlib.pyplot as plt

def main():
    # Options parsing.
    parser = OptionParser(usage="%prog -i INPUT [-o OUTPUT]", description="Description: %prog calculate free energy diffrences based on the raw equilibrium data.", epilog="Example: prob2free.py -i data1.dat -d data2.dat -o freediff.dat")
    parser.add_option("-i", "--first_input", help="One column of numerical data as reference.")
    parser.add_option("-d", "--second_input", help="Space separated columns of numerical data.")
    parser.add_option("-o", "--output", default="freediff.dat", help="Output data.")
    (options, args) = parser.parse_args()


    # Input data reading.
    data1 = read_file(options.first_input)
    data2 = read_file(options.second_input)
    dataa1 = np.array(data1)
    dataa2 = np.array(data2)
    print("[" + strftime("%H:%M:%S", localtime()) + "] Data loaded.")

    # Histogram calculation for reference data
    hist1, bin_edges1 = np.histogram(dataa1, bins=100, normed=1)
    new_bins1 = bin_edges1[:-1] + (bin_edges1[1] - bin_edges1[0])/2      #shifting bins centers

    # Initial values for fitting of reference gaussian function
    n = len(dataa1)
    mean = sum(dataa1) / n
    sigma = sum(np.asarray(dataa1 - mean) ** 2) / n
    init_vals1 = [1, mean, sigma]  # for [amp, cen, wid]

    #Fitting procedure
    best_vals1, covar1 = curve_fit(gaussian, new_bins1, hist1, p0=init_vals1)
    max_value1 = gaussian(best_vals1[1], *best_vals1)

    dataout = []
    num_cols = len(dataa2[0][:])
    for i in range(num_cols):
        # Histogram calculation for rest of data
        hist2, bin_edges2 = np.histogram(dataa2[:,i], bins=100, normed=1)
        new_bins2 = bin_edges2[:-1] + (bin_edges2[1] - bin_edges2[0])/2  # shifting bins centers

        # Initial values for fitting of rest of gaussian functions
        n = len(dataa2[:,i])
        mean = sum(dataa2[:,i]) / n
        sigma = sum(np.asarray(dataa2[:,i] - mean) ** 2) / n
        init_vals2 = [1, mean, sigma]  # for [amp, cen, wid]

        # Fitting procedure
        best_vals2, covar2 = curve_fit(gaussian, new_bins2, hist2, p0=init_vals2)
        max_value2 = gaussian(best_vals2[1], *best_vals2)

        # Calculations of free energie profiles
        free_energy1 = -0.6 * np.log(gaussian(new_bins2, *best_vals1) / max_value1)
        #free_energy2 = -0.6 * np.log(gaussian(new_bins2, *best_vals2) / max_value2)

        # Calculations difference of free energies in minima
        diff_free_energy1 = -0.6 * np.log(gaussian(best_vals2[1], *best_vals1) / max_value1)

        # Calculations weighted average difference of free energies
        gaus2 = gaussian(new_bins2, *best_vals2)
        diff_free_energy2 = sum(free_energy1 * gaus2) / sum(gaus2)

        #print(diff_free_energy1, diff_free_energy2)
        dataout.append(diff_free_energy2)


        # Plots
        '''
        plt.plot(new_bins1, hist1, 'b+:', label='data1')
        plt.plot(new_bins1, gaussian(new_bins1, *best_vals1), 'bo:', label='fit1')
        plt.plot(new_bins2, hist2, 'g+:', label='data2')
        plt.plot(new_bins2, gaussian(new_bins2, *best_vals2), 'go:', label='fit2')
        plt.legend()
        plt.show()

        plt.plot(new_bins2, free_energy1, 'b-.', label='free energy1')
        plt.plot(new_bins2, free_energy2, 'g-.', label='free energy2')
        plt.show()
        '''

    np.savetxt(options.output, dataout, fmt='%.6f')
    print("[" + strftime("%H:%M:%S", localtime()) + "] Free energy diffrences data saved as:", options.output)


###################################################################################################

# Definition of the gaussian function.
def gaussian(x, amp, cen, wid):
    return amp * exp(-(x - cen) ** 2 / wid)

# Reads file as a float matrix.
def read_file(input_file):
    f = []
    with open(input_file) as fp:
        for line in fp:
            f.append(line.rstrip().split())
    for row in range(len(f)):
        for i in range(len(f[row])):
            f[row][i] = float(f[row][i])
    return f

###################################################################################################

main()
=======
>>>>>>> 3b5d9d9e3b5c6f2ed3f568608c527f17602e6e56
